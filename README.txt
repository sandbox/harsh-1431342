
Social Circle module
-----------------
by Harsh Kharbanda, giveurmsg@gmail.com

This module provide users a way of organizing his/her friends. Using this 
module, a user can perform the following tasks:

1) Create a new circle.
2) Drag-in a user into the circle.
3) Drag-out a user from the circle.
4) Edit an existing circle.
5) Delete an existing circle.
6) Adding new users in the Public area.
7) On click display of users per circle.
8) Transfer of users from one circle to another.
9) Drag-in of multiple users into the circle.

This README is for interested developers. If you are not interested in 
developing, you may stop reading now.

Installation
------------
1) Download the module and place in your /sites/all/modules/contrib or wherever
you normally install your contrib modules.
2) Enable the module using Administer -> Modules (/admin/modules)
3) Go to /admin/config/people/accounts, under Personalization, provide 'Default
picture' url & set 'Picture display style' to circle-thumb. Save the 
configuration.
4) Go to /admin/structure/flags, Edit 'friend' flag, under Flag access, allow
both authenticated user & administrator to flag & unflag the friend flag. 
Choose the 'Display options' and submit it.

Support
-------
If you experience a problem with social_circle or have a problem, file a
request or issue on the social_circle queue at
http://drupal.org/project/issues/social_circle. DO NOT POST IN THE FORUMS.
Posting in the issue queues is a direct line of communication with the module
authors.

No guarantee is provided with this software, no matter how critical your
information, module authors are not responsible for damage caused by this
software or obligated in any way to correct problems you may experience.
